<?php

  require_once("helper.php");
  // loads mongodb library

  require_once("../vendor/autoload.php");

  $connection = new MongoDB\Client();

  // connect to collection user in db scout
  $scout = $connection->scout;

  $users = $scout->users;
  $requirements = $scout->requirements;
  $userReqs = $scout->userReqs;


  session_start();



    // redirect user to login page if not logged in and is not on specified pages
    if (!in_array($_SERVER["PHP_SELF"], ["/login.php"])) {

        if (empty($_SESSION["id"])) {
            redirect("/login.php");
        }

    }




    // regular users get yelled at if try to access certain pages
    if (!authorized(["super"])) {
        // history is checked elsewhere as there are different versions of that page
        if (in_array($_SERVER["PHP_SELF"], ["/update.php"])) {


            header("HTTP/1.0 401 Unauthorized");

            render("401.php", "401 Error");


              exit;
        }

    }








?>
