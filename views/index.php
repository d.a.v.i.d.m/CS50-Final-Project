



<div class="container">

<h1 id="header" class="headline"> Bulk Update </h1>






<form role="form" id="reqForm" class="form-horizontal" name="reqForm" action="<?php echo htmlspecialchars('../update.php'); ?>" method="post">
  <fieldset>
    <div class="form-group">
      <label> Select Rank: </label>
      <select id="ranks">
        <option value="Scout">Scout</option>
        <option value="Tenderfoot">Tenderfoot</option>
        <option value="SecondClass">Second Class</option>
        <option value="FirstClass">First Class</option>
     </select>
     <?= req();  ?>
   </div>



  <h3 class="headline"> Requirements: <?= req();  ?> </h3>




    <div id="reqs" class="row"></div>


    <h3 class="headline"> People: <?= req();  ?> </h3>


    <div class="form-group">
      <label> Sort By: </label>
      <select id="sortBy">
        <option value="lastName">Last Name</option>
        <option value="firstName">First Name</option>
        <option value="_id">Date Added</option>
     </select>
   </div>


    <div id="people" class="row"></div>




    <div class="form-group">
    <label for="date"> Date </label>
    <input type="date" name="date" id="date" placeholder="MM/DD/YYYY" />
    <a id="dateInfo">  ? </a>
        <div id="infoDiv"> Optional date formatted MM/DD/YYYY. <br /> If not specified, it will be the current date. </div>
</div>



     <input type="submit" name="submitSignOff" id="submit" value="Submit" />


  </fieldset>

</form>

</div>
