<div class='container-fluid'>

    <h1 id="historyHeader"> History </h1>

<form role="form" class="form-responsive" name="individualForm" action="<?php echo htmlspecialchars('../history.php'); ?>" method="post">


    <div class="form-group">
      <label> Select Rank: </label>
      <select id="historyRanks">
        <option value="Scout">Scout</option>
        <option value="Tenderfoot">Tenderfoot</option>
        <option value="SecondClass">Second Class</option>
        <option value="FirstClass">First Class</option>
     </select>
   </div>


   <div class="form-group hideReg">
     <label> Select Person: </label>
     <select id="individuals" name='individuals'>

    </select>
   </div>


   <div class="form-group hideReg">
     <label> Show Requirements of Rank: </label>
     <select id="indRanks">
       <option value="Scout">Scout</option>
       <option value="Tenderfoot">Tenderfoot</option>
       <option value="SecondClass">Second Class</option>
       <option value="FirstClass">First Class</option>
    </select>
  </div>



    <table class='table table-striped table-hover'>
      <thead>
        <tr>
          <th>Name</th>
          <th>Status</th>
          <th> Date Completed </th>
          <th> Signed By </th>
        </tr>
      </thead>
      <tbody id="historyContent">


      </tbody>
  </table>


</form>
</div>
