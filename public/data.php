<?php






$users->insertMany([


    ["firstName" => "Jim", "lastName" => "Smith", "type" => "user", "rank" => "Scout", "password" => "hflsdkhfiuvbdslkfhewuifbdskljvblefhuirvndslkjvbsd", "username" => "jSmith", "dateJoined" => "08/31/2016"],



    ["firstName" => "Bob", "lastName" => "Knowle", "type" => "user", "rank" => "Scout", "username" => "bKnowle", "password" => "dsfjiewh8gvibou3489ghrwiubv3o48ytewfvsl", "dateJoined" => "08/01/2016"],


    ["firstName" => "Eric", "lastName" => "West", "type" => "super", "rank" => "ScoutMaster", "username" => "bWest", "password" => "oweyfhnvliugy4oifhnbvdskljv lrihuvlivbeiruh", "dateJoined" => "02/23/2009"],


    ["firstName" => "Quincy", "lastName" => "Bobson", "type" => "user", "rank" => "Tenderfoot", "username" => "qBobson", "password" => "lvnlkdsbvuio34yfoiuwhv834o7t680348fhnl", "dateJoined" => "12/11/2015"],



    ["firstName" => "Sam", "lastName" => "Hew", "type" => "user", "rank" => "Tenderfoot", "username" => "sHew", "password" => "sdlfhlwkvbreio4y89r4fbdslvs", "dateJoined" => "01/01/2013"],


    ["firstName" => "Sally", "lastName" => "Good", "type" => "user", "rank" => "Tenderfoot", "username" => "sGood", "password" => "lsdvblueiaho34yrfhdosiblvoiw4yg7", "dateJoined" => "11/04/2015"
],



    ["firstName" => "David", "lastName" => "Brewport", "type" => "user", "rank" => "Tenderfoot", "username" => "dBrewport", "password" => "SDFWEfi;wovnsldkewfSDFsfh2398", "dateJoined" => "03/22/2016"
],



    ["firstName" => "Samuel", "lastName" => "Green", "type" => "user", "rank" => "FirstClass", "username" => "sGreen", "password" => "SDFsd;lfhsdvSDFwear9834y023", "dateJoined" => "04/21/2012"
],



    ["firstName" => "Joe", "lastName" => "Wajasky", "type" => "user", "rank" => "FirstClass", "username" => "jWajesky", "password" => "SDFsdfsdFSDf435343EWt", "dateJoined" => "03/11/2013"
],




    ["firstName" => "Day", "lastName" => "Fried", "type" => "user", "rank" => "FirstClass", "username" => "dFried", "password" => "dslfhlweGSFewuitoy325rhwef", "dateJoined" => "02/01/2011"
],



    ["firstName" => "Sarah", "lastName" => "Jenkins", "type" => "user", "rank" => "FirstClass", "username" => "sJenkins", "password" => "lksdhflsdFSDf39758032", "dateJoined" => "06/27/2012"
],



    ["firstName" => "Gertrude", "lastName" => "Hope", "type" => "user", "rank" => "FirstClass", "username" => "gHope", "password" => "sdlfsdFwe573208", "dateJoined" => "08/01/2016"
],



    ["firstName" => "Dorry", "lastName" => "Brod", "type" => "user", "rank" => "SecondClass", "username" => "dBrod", "password" => "SDflsdhfewFSdty438593h", "dateJoined" => "05/13/2014"
],



    ["firstName" => "Greg", "lastName" => "Tremain", "type" => "user", "rank" => "SecondClass", "username" => "gTremain", "password" => "lhsdfsDF947832EWTwt", "dateJoined" => "12/01/2014", "username" => "gTremain", "password" => "SDFksdlgsdlg@#423sdFWE", "dateJoined" => "07/09/2012"

],



    ["firstName" => "Forry", "lastName" => "Gold", "type" => "user", "rank" => "SecondClass", "username" => "fGold", "password" => "SDfdsfew323423DSfew", "dateJoined" => "08/01/2016"
]


]);



// works
$inserted = $requirements->InsertMany([
    ['rank' => 'Scout',
     'name'=> '1a',
     'label'=>'Repeat from memory the Scout Oath, Scout Law, Scout motto, and Scout slogan. In your own words, explain their meaning.',
     'year' => '2016'

    ],


    ['rank' => 'Scout',
     'name'=> '1b',
     'label'=>'Explain what Scout spirit is. Describe some ways you have shown Scout spirit by practicing the Scout Oath, Scout Law, Scout motto, and Scout slogan.',
     'year' => '2016'

    ],


    ['rank' => 'Scout',
     'name'=> '1c',
     'label'=>'Demonstrate the Boy Scout sign, salute, and handshake. Explain when they should be used.',
     'year' => '2016'

    ],


    ['rank' => 'Scout',
     'name'=> '1d',
     'label'=>'Describe the First Class Scout badge and tell what each part stands for. Explain the significance of the First Class Scout badge.',
     'year' => '2016'

    ],

    ['rank' => 'Scout',
     'name'=> '1e',
     'label'=>'Repeat from memory the Outdoor Code. In your own words, explain what the Outdoor Code means to you.',
     'year' => '2016'

    ],

    ['rank' => 'Scout',
     'name'=> '1f',
     'label'=>'Repeat from memory the Pledge of Allegiance. In your own words, explain its meaning.',
     'year' => '2016'

    ],

    ['rank' => 'Scout',
     'name'=> '2a',
     'label'=>'Describe how the Scouts in the troop provide its leadership.',
     'year' => '2016'

    ],

    ['rank' => 'Scout',
     'name'=> '2b',
     'label'=>'Describe the four steps of Boy Scout advancement.',
     'year' => '2016'

    ],

    ['rank' => 'Scout',
     'name'=> '2c',
     'label'=>'Describe what the Boy Scout ranks are and how they are earned.',
     'year' => '2016'

    ],

    ['rank' => 'Scout',
     'name'=> '2d',
     'label'=>'Describe what merit badges are and how they are earned.',
     'year' => '2016'

    ],

    ['rank' => 'Scout',
     'name'=> '3a',
     'label'=>'Explain the patrol method. Describe the types of patrols that are used in your troop.',
     'year' => '2016'

    ],

    ['rank' => 'Scout',
     'name'=> '3b',
     'label'=>'Become familiar with your patrol name, emblem, flag, and yell. Explain how these items create patrol spirit.',
     'year' => '2016'

    ],

    ['rank' => 'Scout',
     'name'=> '4a',
     'label'=>'Show how to tie a square knot, two half-hitches, and a taut-line hitch. Explain how each knot is used.',
     'year' => '2016'

    ],

    ['rank' => 'Scout',
     'name'=> '4b',
     'label'=>'Show the proper care of a rope by learning how to whip and fuse the ends of different kinds of rope.',
     'year' => '2016'

    ],

    ['rank' => 'Scout',
     'name'=> '5a',
     'label'=>'Demonstrate your knowledge of pocketknife safety.',
     'year' => '2016'

    ],

    ['rank' => 'Scout',
     'name'=> '6a',
     'label'=>'With your parent or guardian, complete the exercises in the pamphlet "How to Protect Your Children from Child Abuse: A Parents Guide" and earn the Cyber Chip Award for your grade.',
     'year' => '2016'

    ],

    ['rank' => 'Scout',
     'name'=> '7a',
     'label'=>'Since joining the troop and while working on the Scout rank, participate in a Scoutmaster conference.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '1a',
     'label'=>'Present yourself to your leader prepared for an overnight camping trip. Show the personal and camping gear you will use. Show the right way to pack and carry it.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '1b',
     'label'=>'Spend at least one night on a patrol or troop campout. Sleep in a tent you have helped pitch.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '1c',
     'label'=>'Tell how you practiced the Outdoor Code on a campout or outing.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '2a',
     'label'=>'On the campout, assist in preparing one of the meals. Tell why it is important for each patrol member to share in meal preparation and cleanup.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '2b',
     'label'=>'While on a campout, demonstrate the appropriate method of safely cleaning items used to prepare, serve, and eat a meal.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '2c',
     'label'=>'Explain the importance of eating together as a patrol.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '3a',
     'label'=>'Demonstrate a practical use of the square knot.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '3b',
     'label'=>'Demonstrate a practical use of two half-hitches.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '3c',
     'label'=>'Demonstrate a practical use of the taut line hitch.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '3d',
     'label'=>'Demonstrate proper care, sharpening, and use of the knife, saw, and ax. Describe when each should be used.',
     'year' => '2016'

    ],

// to change back to normal (if needed) simply format like the others and php part is automatic, just remember reset userReqs

    ['rank' => 'Tenderfoot',
     'name'=> '4a',
     'label'=>'First Aid for:
      <br/>&bull;&nbsp;&nbsp;Simple cuts and scrapes.
     <br />&bull;&nbsp;&nbsp;Blisters on the hand and foot.
     <br />&bull;&nbsp;&nbsp;Minor (thermal/heat) burns or scalds (superficial, or first degree).
     <br />&bull;&nbsp;&nbsp;Bites or stings of insects or ticks.
     <br />&bull;&nbsp;&nbsp;Venomous snakebite.
     <br />&bull;&nbsp;&nbsp;Nosebleed.
     <br />&bull;&nbsp;&nbsp;Frostbite and sunburn.
     <br />&bull;&nbsp;&nbsp;Choking.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '4b',
     'label'=>'Describe common poisonous or hazardous plants, identify any that grow in your local area or campsite location. Tell how to treat for exposure to them.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '4c',
     'label'=>'Tell what you can do on a campout or other outdoor activity to prevent or reduce the occurrence of injuries or exposure listed above.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '4d',
     'label'=>'Assemble a personal first-aid kit to carry with you on future campouts and hikes. Tell how each item in the kit would be used.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '5a',
     'label'=>'Explain the importance of the buddy system as it relates to your personal safety on outings and in your neighborhood. Use the buddy system while on a troop or patrol outing.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '5b',
     'label'=>'Explain what to do if you become lost on a hike or campout.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '5c',
     'label'=>'Explain the rules of safe hiking, both on the highway and cross-country, during the day and at night.',
     'year' => '2016'

    ],

    // if say '6a.1' instead, mongo treats as object dot notation when updating, messing it up

    ['rank' => 'Tenderfoot',
     'name'=> '6a1',
     'label'=>'Record pushups (60 seconds).',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '6a2',
     'label'=>'Record situps or curl-ups (60 seconds).',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '6a3',
     'label'=>'Record back-saver sit-and-reach (cm).',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '6a4',
     'label'=>'Record time for 1 mile walk/run.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '6b',
     'label'=>'Develop and describe a plan for improvement in each of the activities listed in Tenderfoot requirement 6a. Keep track of your activity for at least 30 days.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '6c1',
     'label'=>'Improvement on pushups.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '6c2',
     'label'=>'Improvement on situps or curl-ups.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '6c3',
     'label'=>'Improvement on back-saver sit-and-reach (cm).',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '6c4',
     'label'=>'Improvement on 1 mile walk/run.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '7a',
     'label'=>'Demonstrate how to display, raise, lower, and fold the U.S. flag.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '7b',
     'label'=>'Participate in a total of one hour of service in one or more service projects approved by your Scoutmaster. Explain how your service to others relates to the Scout slogan and Scout motto.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '8a',
     'label'=>'Describe the steps in Scouting\'s Teaching EDGE method. Use the Teaching EDGE method to teach another person how to tie the square knot.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '9a',
     'label'=>'Demonstrate Scout spirit by living the Scout Oath and Scout Law. Tell how you have done your duty to God and how you have lived four different points of the Scout Law in your everyday life.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '10a',
     'label'=>'While working toward Tenderfoot rank, and after completing the Scout rank requirement to do the same, participate in a Scoutmaster conference.',
     'year' => '2016'

    ],

    ['rank' => 'Tenderfoot',
     'name'=> '11a',
     'label'=>'Successfully complete your board of review for the Tenderfoot rank.',
     'year' => '2016'

    ],


    ['rank' => 'SecondClass',
     'name'=> '1a',
     'label'=>'Since joining, participate in five separate troop/patrol activities, three of which include overnight camping. These five activities do not include troop or patrol meetings. On at least two of the three campouts, spend the night in a tent that you pitch or other structure that you help erect (such as a lean-to, snow cave, or tepee.)',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '1b',
     'label'=>'Explain the principles of Leave No Trace, and tell how you practiced them while on a campout or outing. This outing must be different from the one used for the Tenderfoot requirement.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '1c',
     'label'=>'On one of these campouts, select a location for your patrol site and recommend it to your patrol leader, senior patrol leader, or troop guide. Explain what factors you should consider when choosing a patrol site and where to pitch a tent.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '2a',
     'label'=>'Explain when it is appropriate to use a fire for cooking or other purposes and when it would not be appropriate to do so.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '2b',
     'label'=>'Use a knife, saw, and axe to prepare tinder, kindling, and fuel wood for a cooking fire.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '2c',
     'label'=>'At an approved outdoor location and time, use the tinder, kindling, and fuel wood from the previous requirement to demonstrate how to build a fire. Unless prohibited by local fire restrictions, light the fire. After allowing the flames to burn safely for at least two minutes, safely extinguish the flames with minimal impact to the fire site.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '2d',
     'label'=>'Explain when it is appropriate to use a lightweight stove and when it is appropriate to use a propane stove. Set up a lightweight stove or propane stove. Light the stove, unless prohibited by local fire restrictions. Describe the safety procedures for using these types of stoves.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '2e',
     'label'=>'On one campout, plan and cook one hot breakfast or lunch, selecting foods from MyPlate or the current USDA nutrition model. Explain the importance of good nutrition. Demonstrate how to transport, store, and prepare the foods you selected.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '2f',
     'label'=>'Demonstrate how to tie the sheet bend knot. Describe a situation in which you would use this knot.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '2g',
     'label'=>'Demonstrate how to tie the bowline knot. Describe a situation in which you would use this knot.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '3a',
     'label'=>'Demonstrate how a compass works and how to orient a map. Use a map to point out and tell the meaning of five map symbols.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '3b',
     'label'=>'Using a compass and a map together, take a five-mile hike (or 10 miles by bike) approved by your adult leader and your parent or guardian.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '3c',
     'label'=>'Describe some hazards or injuries that you might encounter on your hike and what you can do to help prevent them.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '3d',
     'label'=>'Demonstrate how to find directions during the day and at night without using a compass or an electronic device.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '4a',
     'label'=>'Identify or show evidence of at least ten kinds of wild animals (such as birds, mammals, reptiles, fish, mollusks) found in your local area or camping location. You may show evidence by tracks, signs, or photographs you have taken.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '5a',
     'label'=>'Tell what precautions must be taken for a safe swim.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '5b',
     'label'=>'Demonstrate your ability to pass the BSA beginner test.  Jump feetfirst into water over your head in depth, level off and swim 25 feet on the surface, stop, turn sharply, resume swimming, then return to your starting place.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '5c',
     'label'=>'Demonstrate water rescue methods by reaching with your arm or leg, by reaching with a suitable object, and by throwing lines and objects.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '5d',
     'label'=>'Explain why swimming rescues should not be attempted when a reaching or throwing rescue is possible. Explain why and how a rescue swimmer should avoid contact with the victim.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '6a',
     'label'=>'First Aid for:
       <br />&bull;&nbsp;&nbsp;Object in the eye.
     <br />&bull;&nbsp;&nbsp;Bite of a warm blooded animal.
     <br />&bull;&nbsp;&nbsp;Puncture wounds from a splinter, nail, and fishhook.
     <br />&bull;&nbsp;&nbsp;Serious burns (partial thickness, or second degree).
     <br />&bull;&nbsp;&nbsp;Heat exhaustion.
     <br />&bull;&nbsp;&nbsp;Shock.
     <br />&bull;&nbsp;&nbsp;Heatstroke, dehydration, hypothermia, and hyperventilation.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '6b',
     'label'=>'Show what to do for "hurry" cases of stopped breathing, stroke, severe bleeding, and ingested poisoning.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '6c',
     'label'=>'Tell what you can do while on a campout or hike to prevent or reduce the occurrence of the injuries listed in the previous two requirements.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '6d',
     'label'=>'Explain what to do in case of accidents that require emergency response in the home and the backcountry. Explain what constitutes an emergency and what information you will need to provide to a responder.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '6e',
     'label'=>'Tell how you should respond if you come upon the scene of a vehicular accident.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '7a',
     'label'=>'After competing Tenderfoot requirement for improvement in the exercises listed there, be physically active at least 30 minutes a day for five days a week for four weeks. Keep track of your activities.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '7b',
     'label'=>'Share your challenges and successes in completing the previous requirement. Set a goal for continuing to include physical activity as part of your daily life and develop a plan for doing so.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '7c',
     'label'=>'Participate in a school, community, or troop program on the dangers of using drugs, alcohol, and tobacco, and other practices that could be harmful to your health. Discuss your participation in the program with your family, and explain the dangers of substance addictions. Report to your Scoutmaster or other adult leader in your troop about which parts of the Scout Oath and Law relate to what you learned.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '8a',
     'label'=>'Participate in a flag ceremony for your school, religious institution, chartered organization, community, or Scouting activity.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '8b',
     'label'=>'Explain what respect is due the flag of the United States.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '8c',
     'label'=>'With your parents or guardian, decide on an amount of money that you would like to earn, based on the cost of a specific item you would like to purchase. Develop a plan written plan to earn the amount agreed upon and follow that plan; it is acceptable to make changes to your plan along the way. Discuss any changes made to your original plan and whether you met your goal.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '8d',
     'label'=>'At a minimum of three locations, compare the cost of the item for which you are saving to determine the best place to purchase it. After completing the previous requirement, decide if you will use the amount that you earned as originally intended, save all or part of it, or use it for another purpose.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '8e',
     'label'=>'Participate in two hours of service through one or more service projects approved by your Scoutmaster. Tell how your service to others relates to the Scout Oath.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '9a',
     'label'=>'Explain the three R\'s of personal safety and protection.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '9b',
     'label'=>'Describe bullying; tell what the appropriate response is to one who might be bullying you or bullying another person.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '10a',
     'label'=>'Demonstrate scout spirit by living the Scout Oath and Scout Law. Tell how you have done your duty to God and how you have lived four different points of the Scout Law (not to include those used for the Tenderfoot requirement to do the same) in your everyday life.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '11a',
     'label'=>'While working toward Second Class rank, and after completing the Tenderfoot requiremement to do the same, participate in a Scoutmaster conference.',
     'year' => '2016'

    ],

    ['rank' => 'SecondClass',
     'name'=> '12a',
     'label'=>'Successfully complete your board of review for the Second Class rank.',
     'year' => '2016'

    ],





    ['rank' => 'FirstClass',
     'name'=> '1a',
     'label'=>'Since joining, participate in 10 separate troop/patrol activities, six of which include overnight camping. These 10 activities do not include troop or patrol meetings. On at least five of the six campouts, spend the night in a tent that you pitch or other structure that you help erect. (such as a lean-to, snow cave, or tepee.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '1b',
     'label'=>'Explain each of the principles of Tread Lightly! and tell how you practiced them while on a campout or outing. This outing must be different from the one used for the Tenderfoot and Second Class requirement to do similiar.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '2a',
     'label'=>'Help plan a menu for one of the above campouts that includes at least one breakfast, one lunch, and one dinner and that requires cooking at least two of the meals. Tell how the menu includes the foods from MyPlate or the current USDA nutrition model and how it meets nutritional needs for the planned activity or campout.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '2b',
     'label'=>'Using the menu planned in the previous requirement, make a list showing a budget and food amounts needed to feed three or more boys. Secure the ingredients.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '2c',
     'label'=>'Show which pans, utensils, and other gear will be needed to cook and serve these meals.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '2d',
     'label'=>'Demonstrate the procedures to follow in the safe handling and storage of fresh meats, dairy products, eggs, vegetables, and other perishable food products. Show how to properly dispose of camp garbage, cans, plastic containers, and other rubbish.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '2e',
     'label'=>'On one campout, serve as cook. Supervise your assistant(s) in using a stove or building a cooking fire. Prepare the breakfast, lunch, and dinner planned earlier. Supervise the cleanup.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '3a',
     'label'=>'Discuss when you should and should not use lashings.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '3b',
     'label'=>'Demonstrate tying the timber hitch and clove hitch.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '3c',
     'label'=>'Demonstrate tying the square, shear, and diagonal lashings by joining two or more poles or staves together.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '3d',
     'label'=>'Use lashings to make a useful camp gadget or structure.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '4a',
     'label'=>'Using a map and compass, complete an orienteering course that covers at least one mile and requires measuring the height and/or width of designated items (tree, tower, canyon, ditch, etc.)',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '3b',
     'label'=>'Demonstrate how to use a handheld GPS unit, GPS app on a smartphone or other electronic navigation system. Use a GPS to find your current location, a destination of your choice, and the route you will take to get there. Follow that route to arrive at your destination.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '5a',
     'label'=>'Identify or show evidence of at least 10 kinds of native plants found in your local area or campsite location. You may show evidence by fallen leaves or fallen fruit that you find in the field, or as part of a collection you have made, or by photographs you have taken.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '5b',
     'label'=>'Identify two ways to obtain a weather forecast for an upcoming activity. Explain why weather forecasts are important when planning for an event.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '5c',
     'label'=>'Describe at least three natural indicators of impending hazardous weather, the potential dangerous events that might result from such weather conditions, and the appropriate actions to take.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '5d',
     'label'=>'Describe extreme weather conditions you might encounter in the outdoors in your local geographic area. Discuss how you would determine ahead of time the potential risk of these types of weather dangers, alternative planning considerations to avoid such risks, and how you would prepare for and respond to those weather conditions.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '6a',
     'label'=>'Successfully complete the BSA swimmer test.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '6b',
     'label'=>'Tell what precautions must be taken for a safe trip afloat.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '6c',
     'label'=>'Identify the basic parts of a canoe, kayak, or other boat. Identify the parts of a paddle or an oar.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '6d',
     'label'=>'Describe proper body positioning in a watercraft, depending on the type and size of the vessel. Explain the importance of proper position.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '6e',
     'label'=>'With a helper and a practice victim, show a line rescue both as tender and rescuer. (The practice victim should be approximately 30 feet from shore in deep water.)',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '7a',
     'label'=>'Demonstrate bandages for a sprained ankle and for injuries on the head, the upper arm, and the collarbone.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '7b',
     'label'=>'By yourself and with a partner show how to:
     <br />&bull;&nbsp;&nbsp;Transport a person from a smoke-filled room.
     <br />&bull;&nbsp;&nbsp;Transport for at least 25 yards a person with a sprained ankle.',
     'year' => '2016'

    ],


    ['rank' => 'FirstClass',
     'name'=> '7c',
     'label'=>'Tell the five most common signals of a heart attack. Explain the steps (procedures) in cardiopulmonary resuscitation (CPR).',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '7d',
     'label'=>'Tell what utility services exist in your home or meeting place. Describe potential hazards associated with these utilities, and tell how to respond in emergency situations.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '7e',
     'label'=>'Develop an emergency action plan for your home that includes what to do in case of fire, storm, power outage, or water outage.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '7f',
     'label'=>'Explain how to obtain potable water in an emergency.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '8a',
     'label'=>'After completing Second Class requirement to do the same, be physically active at least 30 minutes every day for five days a week for four weeks. Keep track of your activities.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '8b',
     'label'=>'Share your challenges and successes in completing the previous requirement. Set a goal for continuing to include physical activity as part of your daily life.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '9a',
     'label'=>'Visit and discuss with a selected individual approved by your leader (for example, an elected official, judge, attorney, civil servant, principal, or teacher) the constitutional rights and obligations of a U.S. citizen.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '9b',
     'label'=>'Investigate an environmental issue affecting your community. Share what you learned about that issue with your patrol or troop. Tell what, if anything, could be done by you or your community to address the concern.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '9c',
     'label'=>'On a Scouting or family outing, take note of the trash and garbage you produce. Before your next similar outing, decide how you can reduce, recycle, or repurpose what you take on that outing, and then put those plans into action. Compare your results.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '9d',
     'label'=>'Participate in three hours of service through one or more service projects approved by your Scoutmaster. The project(s) must not be the same service project(s) used for the Tenderfoot and Second Class requirements. Explain how your service to others relates to the Scout Law.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '10a',
     'label'=>'Tell someone who is eligible to join Boy Scouts, or an inactive Boy Scout, about your Scouting activities. Invite him to an outing, activity, service project or meeting. Tell him how to join, or encourage the inactive Boy Scout to become active. Share your efforts with your Scoutmaster or other adult leader.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '11a',
     'label'=>'Demonstrate scout spirit by living the Scout Oath and Scout Law. Tell how you have done your duty to God and how you have lived four different points of the Scout Law (different from those points used for previous ranks) in your everyday life.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '12a',
     'label'=>'While working toward First Class rank, and after completing the Second Class requirement to do the same, participate in a Scoutmaster conference.',
     'year' => '2016'

    ],

    ['rank' => 'FirstClass',
     'name'=> '13a',
     'label'=>'Successfully complete your board of review for the First Class rank.',
     'year' => '2016'

    ]



]);





















 ?>
